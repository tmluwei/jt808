package com.ltmonitor.service;

import java.util.List;

import com.ltmonitor.entity.Department;
import com.ltmonitor.entity.Terminal;
import com.ltmonitor.entity.VehicleData;

public interface IVehicleService {

	public abstract List<VehicleData> getVehicleListByDepId(
			List<Integer> depIdList);
	
	public VehicleData getVehicleData(int vehicleId);

	Department getDepartmentByPlateNo(String plateNo);
	//保存车辆信息，并判断是否重复
	void saveVehicleData(VehicleData vd) throws Exception;

	VehicleData getVehicleBySimNo(String simNo);

	Terminal getTerminal(int terminalId);

	Terminal getTerminalByTermNo(String terminalNo);

	VehicleData getVehicleByPlateNo(String plateNo);
	/**
	 * 保存终端信息,判断终端编号是否重复
	 * @param t
	 * @throws Exception
	 */
	 void saveTerminal(Terminal t) throws Exception;

}
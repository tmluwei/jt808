package com.ltmonitor.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
/**
 * 在线统计
 * @author DELL
 *
 */
public class OnlineStatistic extends TenantEntity{
	
	 //统计类型，便于根据不同的类型，生成不同的图标
    public static int BY_HOUR = 0;
    public static int BY_MINUTE = 1; //每五分钟一次，或30分钟一次.
    public static int BY_DAY = 2;
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "statId", unique = true, nullable = false)
	private int entityId;
	public  int getEntityId() {
		return entityId;
	}
	public  void setEntityId(int value) {
		entityId = value;
	}
	

	private int depId  ;

    private String depName ;

    private int vehicleNum ;

    private int onlineNum ;

    private double onlineRate ;

    private Date statisticTime ;

    private int parentDepId ;

    private int interval ;
	
    public int getDepId() {
		return depId;
	}
	public void setDepId(int depId) {
		this.depId = depId;
	}

	public String getDepName() {
		return depName;
	}
	public void setDepName(String depName) {
		this.depName = depName;
	}

	public int getVehicleNum() {
		return vehicleNum;
	}
	public void setVehicleNum(int vehicleNum) {
		this.vehicleNum = vehicleNum;
	}

	public int getOnlineNum() {
		return onlineNum;
	}
	public void setOnlineNum(int onlineNum) {
		this.onlineNum = onlineNum;
	}

	public double getOnlineRate() {
		return onlineRate;
	}
	public void setOnlineRate(double onlineRate) {
		this.onlineRate = onlineRate;
	}

	public Date getStatisticTime() {
		return statisticTime;
	}
	public void setStatisticTime(Date statisticTime) {
		this.statisticTime = statisticTime;
	}

	public int getParentDepId() {
		return parentDepId;
	}
	public void setParentDepId(int parentDepId) {
		this.parentDepId = parentDepId;
	}

	public int getInterval() {
		return interval;
	}
	public void setInterval(int interval) {
		this.interval = interval;
	}


}
